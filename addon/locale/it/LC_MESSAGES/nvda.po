# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: 'Instant-Translate' '2.1'\n"
"Report-Msgid-Bugs-To: nvda-translations@freelists.org\n"
"POT-Creation-Date: 2013-02-09 18:30+0100\n"
"PO-Revision-Date: 2013-03-05 12:11+0100\n"
"Last-Translator: Simone Dal Maso <simone.dalmaso@juvox.it>\n"
"Language-Team: Simone Dal Maso <simone.dalmaso@juvox.it>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.5\n"

#. Translators: name of the option in the menu.
msgid "Instant Translate Settings..."
msgstr "Impostazioni di Instant Translate..."

#. Translators: tooltip text for the menu item.
msgid "Select languages to be used for translation."
msgstr "Selezionare la lingua da utilizzare per la traduzione"

#. Translators: message presented when user presses the shortcut key for translating clipboard text but the clipboard is empty.
msgid "There is no text on the clipboard"
msgstr "Non è presente testo negli appunti."

#. Translators: Message presented when clipboard text (to be translated) is too long (more than a set limit).
#, python-format
msgid ""
"The clipboard contains a large portion of text. It is %s characters long. "
"The limit is 350 characters."
msgstr ""
"Gli appunti contengono una grande porzione di testo. Si tratta di %s "
"caratteri. Il limite è di 350 caratteri."

#. Translators: message presented in input help mode, when user presses the shortcut keys for this addon.
msgid ""
"Translates clipboard text from one language to another using Google "
"Translate."
msgstr ""
"Traduce il testo presente negli appunti da una lingua ad un'altra tramite "
"Google Translate"

#. Translators: user has pressed the shortcut key for translating selected text, but no text was actually selected.
msgid "no selection"
msgstr "Nessuna selezione"

#. Translators: Message presented when selected text (to be translated) is too long (more than a set limit).
#, python-format
msgid ""
"The selection contains a large portion of text. It is %s characters long. "
"The limit is 350 characters."
msgstr ""
"La selezione contiene una grande quantità di testo. Il totale ammonta a %s "
"caratteri"

#. Translators: message presented in input help mode, when user presses the shortcut keys for this addon.
msgid ""
"Translates selected text from one language to another using Google Translate."
msgstr ""
"Traduce il testo selezionato da una lingua ad un'altra tramite Google "
"Translate"

#. Translators: Message presented when the given text (from selected or clipboard) cannot be translated.
msgid "Error translating text. See log for details"
msgstr ""
"Errore nella traduzione del testo. Vedere il log per ulteriori informazioni"

#. Translators: name of the dialog.
msgid "Instant Translate Settings"
msgstr "Impostazioni di Instant Translate"

#. Translators: Help message for a dialog.
msgid "Select translation source and target language:"
msgstr "Selezionare la sorgente di traduzione e la lingua di destinazione:"

#. Translators: A setting in instant translate settings dialog.
msgid "Source language:"
msgstr "Lingua di origine:"

#. Translators: A setting in instant translate settings dialog.
msgid "Target language:"
msgstr "Lingua desiderata:"

#. Translators: An option to automatically detect source language for translation.
msgid "Automatically detect language"
msgstr "Rileva lingua automaticamente"

#. Translators: The name of a language supported by this add-on.
msgid "Arabic"
msgstr "Araba"

#. Translators: The name of a language supported by this add-on.
msgid "Bangla"
msgstr "Bangla"

#. Translators: The name of a language supported by this add-on.
msgid "Welsh"
msgstr "Gallese"

#. Translators: The name of a language supported by this add-on.
msgid "Esperanto"
msgstr "Esperanto"

#. Translators: The name of a language supported by this add-on.
msgid "Gujarati"
msgstr "Gujarati"

#. Translators: The name of a language supported by this add-on.
msgid "Creole Haiti"
msgstr "Creole Haiti"

#. Translators: The name of a language supported by this add-on.
msgid "Armenian"
msgstr "Armena"

#. Translators: The name of a language supported by this add-on.
msgid "Latin"
msgstr "Latina"

#. Translators: The name of a language supported by this add-on.
msgid "Norwegian"
msgstr "Norvegese"

#. Translators: The name of a language supported by this add-on.
msgid "Serbian (Latin)"
msgstr "Serba"

#. Translators: The name of a language supported by this add-on.
msgid "Swahili"
msgstr "Swahili"

#. Translators: The name of a language supported by this add-on.
msgid "Tagalog"
msgstr "Tagalog"

#. Translators: The name of a language supported by this add-on.
msgid "Yiddish"
msgstr "Yiddish"

#. Translators: The name of a language supported by this add-on.
msgid "Chinese (Simplified)"
msgstr "Cinese (semplificato)"

#. Translators: The name of a language supported by this add-on.
msgid "Chinese (Traditional)"
msgstr "Cinese (tradizionale)"

#. Add-on description
#. TRANSLATORS: Summary for this add-on to be shown on installation and add-on informaiton.
msgid ""
"Instant Translate - Translates given text using the Google Translate service."
msgstr ""
"Instant Translate - Traduce il testo selezionato tramite il servizio Google "
"Translate."

#. Add-on description
#. Translators: Long description to be shown for this add-on on installation and add-on information
msgid ""
"This addon translates selected or clipboard text using the Google Translate "
"service and presents it. Press NVDA+Shift+T to translate selected text. "
"Press NVDA+Shift+Y to translate clipboard text."
msgstr ""
"Questo componente aggiuntivo traduce il testo selezionato o presente negli "
"Appunti utilizzando il servizio Google Translate e lo annuncia. ;Premere "
"NVDA + Shift + T per tradurre il testo selezionato. ;Premere NVDA + Shift + "
"Y per tradurre il testo negli appunti."

#~ msgid "Select Languages from which text will be translated from, and into."
#~ msgstr "Seleziona la lingua da e verso cui si desidera tradurre."

#~ msgid ""
#~ "Select language from which you want to translate from and into and press "
#~ "ok."
#~ msgstr "Seleziona la lingua da cui si desidera tradurre e premere ok."

#~ msgid "Into Language:"
#~ msgstr "Alla lingua:"

#~ msgid ""
#~ "This addon translates selected text via Google Translator and says it. "
#~ "Press NVDA+Shift+T for execute translation operation on selected text in "
#~ "Reech edit fields. This addon was been repacked and optimized for "
#~ "executing without standalone Python by Outsider <outsidepro@rambler.ru>."
#~ msgstr ""
#~ "Questo componente aggiuntivo traduce il testo selezionato tramite Google "
#~ "Translator e lo annuncia. Premere NVDA + Shift + T per eseguire "
#~ "l'operazione di traduzione sul testo selezionato in un campo editazione. "
#~ "Questo componente aggiuntivo è stato ottimizzato per l'esecuzione senza "
#~ "Python standalone da Outsider <outsidepro@rambler.ru>."
