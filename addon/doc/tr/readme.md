# instantTranslate #

* Geliştiriciler: Alexy Sadovoy, ruslan, Beqa Gozalishvili and Diğer nvda
  gönüllüleri.
* İndir [versiyon 2.2beta2][1]

Bu eklenti seçilen ya da panoya kopyalanmış olan metnin tercümesi için
kullanılır. Google Translate servisi kullanılmaktadır.

## Dillerin ayarlanması ##

Kaynak ve hedef dilleri ayarlamak için NVDA tercihler menüsünün altındaki
anlık çeviri ayarları iletişim kutusunu kullanın.  Burada iki seçim kutusu
bulunmaktadır. biri "Kaynak dil" diğeri de "hedef dil" başlıklıdır.
Seçimlerinizi yapıp tamam düğmesine basın.

## Eklentinin kullanımı ##

Eklenti iki yolla kullanılabilir :

1. Örneğin shift ve yön tuşlarıyla metin seçtikten sonra Shift+NvDA+T
   tuşlarına basarak seçili metni çevirebilirsiniz. Metin çevrilir ve
   seslendirilir, tabii kullanmakta olduğunuz sentezleyici hedef dili
   destekliyorsa.
2. Panoya metin kopyalayın. Panodaki metnin hedef dile tercümesi için
   Shift+NvDA+Y tuşlarına basın.

## 2.2 için değişiklikler ##
* karakter sayısı 1500'e yükseltildi
* Anında Çeviri Ayarlar menü öğesi eklendi kısayol t
* Çeviri sonuçlarının kopyalanmasıyla ilgili bir onay kutusu eklendi.
* Yapılandırma dosyasını konfigürasyon dizininin kökünde bulundur
* Yeni diller: Aragonese, Arapça, Brezilya Portekizcesi, İspanyolca,
  Slovence, Slovakça, Lehçe, Nepal, Korece, Japonca, İtalyanca, Macarca,
  Almanca, Galiçyaca, Fransızca, Fince Hırvatça, Hollandaca, Tamil, Türkçe.

## 2.1 için değişiklikler ##
* NVDA + shift + y basılınca Şimdi eklenti panodaki metni çevirebilir.

## 2.0 için değişiklikler ##
* kaynak ve hedef dil seçebileceğiniz gui yapılandırıcı eklendi.
* Tercihler menüsü altına eklenti menü öğesi eklendi.
* Ayarlar şimdi ayrı bir yapılandırma dosyasına yazılıyor.
* Çeviri sonuçları artık otomatik olarak gelecek kullanımlar için panoya
  kopyalanır.

## 1.0 için değişiklikler ##
* İlk sürümü.

[[!tag dev]]

[1]: http://addons.nvda-project.org/files/get.php?file=it
